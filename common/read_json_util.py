import os
import json

from 黑马测试.接口自动化框架.config import BASE_PATH


def build_data(json_file):
    with open(BASE_PATH + "\\data\\"+json_file, "r", encoding="utf-8") as f:
        login_data_json = json.load(f)
        new_list = []
        for data in login_data_json:
            tmp = tuple(data.values())
            new_list.append(tmp)

        return new_list


print(build_data("add_emp.json"))
print(build_data("login_case.json"))
