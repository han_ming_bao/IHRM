def ihrm_login_assert(self,res,status_code,success,code,message):
    self.assertEqual(status_code, res.status_code)
    self.assertEqual(success, res.json().get('success'))
    self.assertEqual(code, res.json().get('code'))
    self.assertIn(message, res.json().get("message"))