import requests


def get_headers():
    url = "http://ihrm-test.itheima.net/api/sys/login"
    data = {"mobile": "13800000002", "password": "123456"}

    res = requests.post(url=url, json=data)
    token = res.json().get("data")
    header = {"Content-Type": "application/json",
              "Authorization": "Bearer " + token}
    return header

# print(get_headers())
