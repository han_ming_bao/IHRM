import requests


class TestIhrmLoginAPi(object):
    @classmethod
    def login(cls, data):
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/99.0.4844.84 '
                          'Safari/537.36 '


        }
        url = "http://ihrm-test.itheima.net/api/sys/login"

        res = requests.post(url=url, headers=headers, json=data)
        return res


if __name__ == '__main__':
    request_param = {"mobile": "13800000002", "password": "123456"}
    print(TestIhrmLoginAPi.login(request_param).json())
