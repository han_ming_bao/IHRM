import requests


class IhrmEmpCurd(object):

    # 添加员工
    @classmethod
    def ihrm_emp_add(cls, headers, data):
        url = "http://ihrm-test.itheima.net/api/sys/user"
        res = requests.post(url, headers=headers, json=data)
        return res

    # 删除员工
    @classmethod
    def ihrm_emp_delete(cls, headers, emp_id):
        url = "http://ihrm-test.itheima.net/api/sys/user/" + emp_id
        res = requests.delete(url, headers=headers)
        return res

    # 修改员工
    @classmethod
    def ihrm_emp_update(cls, headers, emp_id, modify_data):
        url = "http://ihrm-test.itheima.net/api/sys/user/" + emp_id
        res = requests.put(url, headers=headers, json=modify_data)
        return res

    # 查询员工
    @classmethod
    def ihrm_emp_query(cls, headers, emp_id):
        url = "http://ihrm-test.itheima.net/api/sys/user/" + emp_id
        res = requests.get(url, headers=headers)
        return res


# if __name__ == '__main__':
#     headers = {"Authorization": "Bearer 893d4ea2-a61f-4004-a0b4-a1dfdbf92e85",
#                "Content-Type": "application/json"}
#     data = {
#         "username": "小猪测试",
#         "mobile": "13678187678",
#         "workNumber": "9527"
#     }
#     print(IhrmEmpCurd.ihrm_emp_add(headers, data).json())
#     print(IhrmEmpCurd.ihrm_emp_update(headers,"1521049935330525184",data).json())
    # print()