import unittest

from htmltestreport import HTMLTestReport

from 黑马测试.接口自动化框架.config import BASE_PATH
from 黑马测试.接口自动化框架.script.test_ihrm_add import TestIhrmCrud
from 黑马测试.接口自动化框架.script.test_ihrm_login import TestIhrmLogin

suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestIhrmLogin))
suite.addTest(unittest.makeSuite(TestIhrmCrud))
# runner = unittest.TextTestRunner()
# runner.run(suite)
# 3. 创建 HTMLTestReport 类实例。 runner
runner = HTMLTestReport(BASE_PATH+"/report/ihrm.html", description="登录与员工添加测试", title="第一次测试") # 相对路径
# 4. runner 调用 run(), 传入 suite
runner.run(suite)
