import unittest

from 黑马测试.接口自动化框架.common.get_header import get_headers
from 黑马测试.接口自动化框架.common.read_json_util import build_data
from 黑马测试.接口自动化框架.api.ihrm_emp_curd_api import IhrmEmpCurd
from 黑马测试.接口自动化框架.common.assert_until import ihrm_login_assert
from 黑马测试.接口自动化框架.common.db_util import DBUtils

from 黑马测试.接口自动化框架.config import TEL
from parameterized import parameterized


class TestIhrmCrud(unittest.TestCase):
    headers = {}

    @classmethod
    def setUpClass(cls) -> None:
        cls.headers = get_headers()
        print(cls.headers)

    # 解决手机号唯一
    def setUp(self) -> None:
        sql = f"delete  from bs_user where mobile='{TEL}'"
        DBUtils.uid_db(sql)

    def tearDown(self) -> None:
        sql = f"delete  from bs_user where mobile='{TEL}'"
        DBUtils.uid_db(sql)

    @parameterized.expand(build_data("add_emp.json"))
    def test01_add_emp(self, desc, req_body, status_code, success, code, message):
        # headers = {"Authorization": "Bearer 893d4ea2-a61f-4004-a0b4-a1dfdbf92e85",
        #            "Content-Type": "application/json"}
        print(f"请求数据：{desc} {req_body} {status_code} {success} {code} {message}")

        res = IhrmEmpCurd.ihrm_emp_add(self.headers, req_body)

        ihrm_login_assert(self, res, status_code, success, code, message)


# if __name__ == '__main__':
#     suite = unittest.TestSuite()
#     suite.addTest(unittest.makeSuite(TestIhrmCrud))
#     runner = unittest.TextTestRunner()
#     runner.run(suite)
    # print(build_data("add_emp.json"))
    # unittest.main(TestIhrmCrud)
