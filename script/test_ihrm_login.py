import logging
import unittest
from 黑马测试.接口自动化框架.api.ihrm_login_api import TestIhrmLoginAPi
from 黑马测试.接口自动化框架.common.assert_until import ihrm_login_assert

from parameterized import parameterized

from 黑马测试.接口自动化框架.common.log_use import init_log_config
from 黑马测试.接口自动化框架.common.read_json_util import build_data
from 黑马测试.接口自动化框架.config import BASE_PATH


class TestIhrmLogin(unittest.TestCase):
    init_log_config(BASE_PATH + "/log/login_log.log")

    @parameterized.expand(build_data("login_case.json"))
    def test01_login(self, desc, req_body, status_code, success, code, message):
        print(f"请求数据：{desc} {req_body} {status_code} {success} {code} {message}")
        res = TestIhrmLoginAPi.login(req_body)
        ihrm_login_assert(self, res, status_code, success, code, message)
        logging.DEBUG(f"请求数据：{desc} {req_body} {status_code} {success} {code} {message}")
        # self.assertIn()


if __name__ == '__main__':
    unittest.main(TestIhrmLogin)
