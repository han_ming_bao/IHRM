import unittest

from 黑马测试.接口自动化框架.api.ihrm_emp_curd_api import IhrmEmpCurd
from 黑马测试.接口自动化框架.common.db_util import DBUtils
from 黑马测试.接口自动化框架.common.get_header import get_headers


class IhrmQuery(unittest.TestCase):
    headers = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.headers = get_headers()
        print(cls.headers)

    def setUp(self) -> None:
        sql = "insert into  bs_user(id,mobile,username) values('1075383135459430401','13289000001','随便写的名字')"
        DBUtils.uid_db(sql)

    def tearDown(self) -> None:
        sql = f"delete  from bs_user where mobile='1075383135459430401'"
        DBUtils.uid_db(sql)

    def test01_query(self):
        res = IhrmEmpCurd.ihrm_emp_query(headers=self.headers, emp_id="1075383135459430401")
        print(res.json())
